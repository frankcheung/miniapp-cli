const merge = require('webpack-merge');
const baseConfig = require('./webpack.base.conf');
const FriendlyErrorsPlugin = require('friendly-errors-webpack-plugin');

module.exports = merge(baseConfig, {
  mode: 'development',
  optimization: {
    namedModules: true
  },
  watch: true,
  watchOptions: {
    aggregateTimeout: 300,
    poll: 1000,
    
  },
  devtool: 'cheap-module-source-map',
  plugins: [
    new FriendlyErrorsPlugin()
  ],
});