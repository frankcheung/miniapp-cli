const path = require("path");
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const CopyWebpackPlugin = require("copy-webpack-plugin");
const MiniappEntryPlugin = require('miniapp-entry-plugin');

module.exports = {
  context: path.resolve(process.cwd(), 'src'),
  entry: {
    app: path.resolve(process.cwd(), "src/app.js")
  },
  devtool: 'none',
  output: {
    path: path.resolve(process.cwd(), "dist"),
    filename: "[name].js"
  },
  resolve: {
    extensions: ['.js']
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: [
          {
            loader: 'babel-loader',
            options: {
              cacheDirectory: true
            }
          }
        ]
      },
      {
        test: /\.(sc|sa|le|c)ss$/,
        exclude: /node_modules/,
        oneOf: [
          {
            resource: /\.(sc|sa|c)ss$/,
            use: [
              {
                loader: 'file-loader',
                options: {
                  name: '[path][name].wxss',
                }
              },
              {
                loader: 'postcss-loader'
              },
              {
                loader: 'sass-loader'
              }
            ]
          },
          {
            resource: /\.(le|c)ss$/,
            use: [
              {
                loader: 'file-loader'
              },
              {
                loader: 'postcss-loader'
              },
              {
                loader: 'less-loader'
              }
            ]
          },
        ]
      },
      {
        test: /\.wxss$/,
        type: 'javascript/auto',
        loader: 'file-loader'
      }
    ]
  },
  plugins: [
    new CleanWebpackPlugin({
      cleanOnceBeforeBuildPatterns: ['**/*'],
      cleanStaleWebpackAssets: false
    }),
    new CopyWebpackPlugin([
      {
        from: "./",
        to: path.resolve(process.cwd(), "dist"),
        context: path.resolve(process.cwd(), "src"),
        ignore: ['*.js', '*.scss']
      }
    ]),
    new MiniappEntryPlugin(),
  ]
};
