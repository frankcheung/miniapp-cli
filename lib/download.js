const downloader = require("download-git-repo");
const path = require("path");
const fs = require("fs");

module.exports = async function download(target) {
  const targetPath = path.resolve(
    process.cwd(),
    path.join(target || ".", ".download-temp")
  );
  // console.log(targetPath);
  // fs.mkdirSync(targetPath);
  return new Promise((resolve, reject) => {
    downloader(
      "direct:https://gitee.com/frankcheung/wx-js-template.git#master",
      targetPath,
      {clone: true},
      err => {
        if (err) {
          reject(err)
        }
        resolve(targetPath);
      }
    );
  });
};
