#!/usr/bin/env node

const program = require('commander');

program.version('1.0.0')
	.usage('<command> [name]')
	.command('init', 'init')
	.command('serve', 'serve')
	.command('build', 'build')
	.parse(process.argv)

