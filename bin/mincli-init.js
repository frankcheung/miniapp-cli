#!/usr/bin/env node

const program = require("commander");
const glob = require("glob");
const path = require("path");
const fs = require('fs-extra');
const downloader = require("../lib/download");
const ora = require('ora');
const logger = require('../lib/logger');

program
  .usage("<project name>")
  .description("start to init a project")
  .parse(process.argv);

async function go(rootName) {
  try {
    const spinner = ora('downloading template');
    spinner.start();
    const temp = await downloader(rootName);
    spinner.stop();
    fs.copySync(temp, rootName);
    fs.removeSync(temp);
    logger.success('Generated "%s".', rootName);
  } catch (e) {
    fs.removeSync(rootName);
    console.log(e);
  }
}

// get project name

let projectName = program.args[0];

if (!projectName) {
  program.help();
  return;
}

const list = glob.sync("*");
let rootName = path.basename(process.cwd()); // resolve current folder name

if (list.length) {
  // check if there's a folder name equals to the project name
  const isFolderExist = list.some(item => {
    const file = path.resolve(process.cwd(), item);
    const isDir = fs.statSync(file).isDirectory();
    return isDir && file.includes(projectName);
  });
  if (isFolderExist) {
    logger.fatal(`${projectName} already exists`);
    return;
  }
  rootName = projectName;
} else if (rootName === projectName) {
  rootName = ".";
} else {
  rootName = projectName;
}

go(rootName);
