#!/usr/bin/env node

const webpack = require('webpack');
const prodConfig = require('../build/webpack.prod.conf');
const statsConfig = {
	colors: true,
	entrypoints: false,
}

webpack(prodConfig, (err, stats) => {
	if (err) throw err;
	console.log(stats.toString(statsConfig));
});


